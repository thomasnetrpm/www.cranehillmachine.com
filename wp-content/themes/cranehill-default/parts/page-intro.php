<section class="page-intro">
  <div class="inner-wrap">
    <h1 class="pi-header"><?php if(is_home()):  ?>Latest Post
               <?php elseif(is_search()): ?>Search Results for '<?php echo get_search_query(); ?>'
                <?php elseif(is_404()): ?>404: Page not found <?php elseif(is_archive()): ?>Archives
           <?php elseif(get_field('alternative_h1')): echo get_field('alternative_h1'); else: the_title(); endif; ?></h1>
  </div>
</section>
<!-- Page intro END-->
</div>
<!-- site-header-wrap END -->
