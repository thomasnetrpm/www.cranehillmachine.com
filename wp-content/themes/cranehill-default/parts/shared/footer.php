<!--Site Footer-->
<footer class="site-footer" role="contentinfo">
  <div class="inner-wrap">
    <div class="sf-primary">
      <p class="sf-company-name">Crane Hill Machine & Fabrication, Inc.</p>
      2476 East Hwy 50, Seymour, IN 47274
      <br><span class="sfp-ph">Phone: <a href="tel:8123583534">812.358.3534</a></span> <span>Fax: 812.358.2351</span>
      <br>Email: <a href="mailto:mroyalty@cranehillmachine.com">mroyalty@cranehillmachine.com</a>
      <p class="sfp-nav">
        <a href="/privacy-policy/">Privacy Policy</a><a href="/sitemap/">Site Map</a>
      </p>
    </div>
    <div class="sf-secondary">
      <p>Copyright&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> All Rights Reserved
      <br>Website by <a href="https://business.thomasnet.com/" target="_blank">Thomas Marketing</a></p>
    </div>
  </div>
</footer>
