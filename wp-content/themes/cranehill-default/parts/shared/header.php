<!--Site Header-->
<div class="site-header-wrap">
  <header class="site-header" role="banner">
    <div class="sh-sticky-wrap">
      <div class="inner-wrap">
        <a href="/" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/crane-hill.png" alt="Site Logo"></a>
        <div class="sh-utility-nav">
          <a href="tel:8123583534" class="sh-ph">812.358.3534</a>
          <a href="mailto:mroyalty@cranehillmachine.com" class="sh-email"><img src="<?php bloginfo('template_url'); ?>/img/ico-email.png" class="sh-email-img"><img src="<?php bloginfo('template_url'); ?>/img/ico-email-hover.png" class="sh-email-img-hover"></a>
          <span class="sh-icons">
              <a href="#menu" class="sh-ico-menu menu-link"><span>Show Menu</span></a>
          </span>
        </div>
        <!--Site Nav-->
        <div class="site-nav-container">
          <div class="snc-header">
            <a href="#" class="close-menu menu-link">Close</a>
          </div>
          <?php wp_nav_menu(array(
      'menu'            => 'Primary nav',
      'container'       => 'nav',
      'container_class' => 'site-nav',
      'menu_class'      => 'sn-level-1',
      'walker'        => new themeslug_walker_nav_menu
      )); ?>
        </div>
        <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>
        <!-- site-nav-container END-->
      </div>
      <!--inner-wrap END-->
    </div>
  </header>
