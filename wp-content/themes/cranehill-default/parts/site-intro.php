<section class="site-intro">
<?php if( have_rows('si_slider') ): ?>
  <div class="si-slider flexslider">
    <ul class="slides">
   <?php while ( have_rows('si_slider') ) : the_row(); ?>
      <li>
      <?php if( get_sub_field('si_slider_image')): 
        $imagelarge = wp_get_attachment_image_src(get_sub_field('si_slider_image'), 'full');
      ?>
        <img src="<?php echo $imagelarge[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
      <?php endif;?>
      </li>
      <?php endwhile; ?>
    </ul>
  </div>
  <?php endif; ?>
  <!-- slider END-->
  <div class="inner-wrap">
    <?php if( get_field('si_header')):?><h1 class="si-header"><?php echo get_field('si_header');?></h1><?php endif;?>
  </div>
</section>
<!-- Site intro END-->
</div>
<!-- site-header-wrap END -->
