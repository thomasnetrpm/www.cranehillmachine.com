<section class="services-section">
  <div class="inner-wrap">
    <?php if( have_rows('ss_left_bucket') ): ?>
    <div class="ss-left">
      <?php while ( have_rows('ss_left_bucket') ) : the_row(); ?>
      <?php if( get_sub_field('ss_title_left')):?>
      <a href="<?php echo get_sub_field('ss_link_left');?>" class="ss-bucket" <?php if( get_sub_field( 'ss_open_in_new_tab_left')):?> target="_blank" <?php endif;?> >
      <?php if( get_sub_field('ss_image_left')): 
        $imagelarge = wp_get_attachment_image_src(get_sub_field('ss_image_left'), 'full');
      ?>
      <figure class="ss-image">
        <img src="<?php echo $imagelarge[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
      </figure>
    <?php endif;?>
      <h3 class="ss-label"><?php echo get_sub_field('ss_title_left');?></h3>
      </a>
      <?php endif;?>
      <?php endwhile; ?>
    </div>
    <?php endif;?>

    <?php if( have_rows('ss_right_bucket') ): ?>
    <div class="ss-right">
      <?php while ( have_rows('ss_right_bucket') ) : the_row(); ?>
      <?php if( get_sub_field('ss_title_right')):?>
      <a href="<?php echo get_sub_field('ss_link_right');?>" class="ss-bucket" <?php if( get_sub_field( 'ss_open_in_new_tab_right')):?> target="_blank" <?php endif;?> >
      <?php if( get_sub_field('ss_image_right')): 
        $imagelarge = wp_get_attachment_image_src(get_sub_field('ss_image_right'), 'full');
      ?>
      <figure class="ss-image">
        <img src="<?php echo $imagelarge[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
      </figure>
    <?php endif;?>
      <h3 class="ss-label"><?php echo get_sub_field('ss_title_right');?></h3>
      </a>
      <?php endif;?>
      <?php endwhile; ?>
    </div>
    <?php endif;?>
  </div>
</section>
