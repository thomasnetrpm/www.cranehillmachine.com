<section class="company-sentence-section">
  <div class="inner-wrap">
    <?php if(get_field('css_text')) : ?>
    <p class="css-text">
      <?php echo get_field('css_text');?>
    </p>
    <?php endif;?>
    <?php if(get_field('css_cta_text')) : ?>
      <a class="btn btn-css-rfq" href="<?php echo get_field('css_cta_link');?>" <?php if(get_field( 'css_open_in_new_tab')) : ?> target="_blank" <?php endif;?> ><?php echo get_field('css_cta_text');?></a>
    <?php endif;?>
  </div>
</section>
